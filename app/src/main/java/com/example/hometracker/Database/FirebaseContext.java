package com.example.hometracker.Database;

import androidx.annotation.NonNull;

import com.example.hometracker.Model.Weather;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FirebaseContext {
    private static FirebaseContext instance;
    private FirebaseDatabase database;
    private FirebaseContext(){
        database = FirebaseDatabase.getInstance();
    }
    public static FirebaseContext getInstance(){
        if(instance == null){
            instance = new FirebaseContext();
        }
        return instance;
    }
    private DatabaseReference myRef;

    public interface FirebaseCallbackCurrent{
        void onSuccess(Weather weather);
        void onFail(String message);
    }

    public interface FirebaseCallbackGas{
        void onSuccess(String gas);
        void onFail(String message);
    }

    public interface FirebaseCallbackAll{
        void onSuccess(ArrayList<Weather> list);
        void onFail(String message);
    }

    public void getCurrent(final FirebaseCallbackCurrent callback){
        myRef = database.getReference("weather1");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String humid = dataSnapshot.child("Humid").getValue().toString();
                String temp = dataSnapshot.child("Temp").getValue().toString();
                Weather weather = new Weather(humid, temp);
                callback.onSuccess(weather);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onFail("Lỗi đường truyền!");
            }
        });
    }

    public void getGasCurrent(FirebaseCallbackGas callback){
        myRef = database.getReference("GasState");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String isGas = dataSnapshot.child("GasState").getValue().toString();
                callback.onSuccess(isGas);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onFail("Lỗi đường truyền!");
            }
        });
    }

    public void getAll(FirebaseCallbackAll callback){
        myRef = database.getReference("weather2");

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ArrayList<Weather> list = new ArrayList<>();
                for(DataSnapshot snapshot: dataSnapshot.getChildren()){
                    String humid = snapshot.child("Humid").getValue().toString();
                    String temp = snapshot.child("Temp").getValue().toString();
                    Weather weather = new Weather(humid, temp);
                    list.add(weather);
                }
                callback.onSuccess(list);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                callback.onFail("Lỗi đường truyền!");
            }
        });
    }
}
