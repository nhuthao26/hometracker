package com.example.hometracker.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.hometracker.Database.FirebaseContext;
import com.example.hometracker.Model.Weather;
import com.example.hometracker.R;
import com.example.hometracker.Service.FirebaseService;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.line_temp)
    LineChart linetemp;

    @BindView(R.id.line_humid)
    LineChart linehumid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        init();
    }

    private void init() {

        FirebaseContext.getInstance().getAll(new FirebaseContext.FirebaseCallbackAll() {
            @Override
            public void onSuccess(ArrayList<Weather> list) {
                ArrayList<Entry> dataValues = new ArrayList<>();
                ArrayList<Entry> dataValues2 = new ArrayList<>();

                for(int i=list.size()-10; i<list.size(); i++){
                    Float temp = Float.valueOf(list.get(i).getTemp());
                    dataValues.add(new Entry(Float.valueOf(i), temp));

                    Float humid = Float.valueOf(list.get(i).getHumid());
                    dataValues2.add(new Entry(Float.valueOf(i), humid));
                }
                initTemp(dataValues);
                initHumid(dataValues2);
            }

            @Override
            public void onFail(String message) {

            }
        });

        FirebaseContext.getInstance().getGasCurrent(new FirebaseContext.FirebaseCallbackGas() {
            @Override
            public void onSuccess(String gas) {
                if (gas.equals("1")){
                    warning();
                }
            }

            @Override
            public void onFail(String message) {

            }
        });

    }

    private void initTemp(ArrayList list){
        LineDataSet lineDataSet = new LineDataSet(list, "Nhiệt độ");
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);
        LineData lineData = new LineData(dataSets);

        linetemp.setDrawGridBackground(true);
        linetemp.setDrawBorders(true);

        lineDataSet.setCircleColor(R.color.colorRed);
        lineDataSet.setColor(R.color.colorRed);

        Description description = new Description();
        description.setText("Biểu đồ nhiệt độ");
        linetemp.setDescription(description);

        linetemp.getXAxis().setEnabled(false);

        Legend legend = linetemp.getLegend();
        LegendEntry legendEntry = new LegendEntry();
        legendEntry.formColor = R.color.colorRed;
        legendEntry.label = "Nhiệt độ(ºC)";
        LegendEntry[] entry = new LegendEntry[1];
        entry[0] = legendEntry;
        legend.setCustom(entry);
        linetemp.setData(lineData);
//        linetemp.animateX(4000);
        linetemp.invalidate();
    }

    private void initHumid(ArrayList list){
        LineDataSet lineDataSet = new LineDataSet(list, "Độ ẩm(%)");
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);
        LineData lineData = new LineData(dataSets);

        linehumid.setDrawGridBackground(true);
        linehumid.setDrawBorders(true);

        Description description = new Description();
        description.setText("Biểu đồ độ ẩm");
        linehumid.setDescription(description);

        linehumid.getXAxis().setEnabled(false);

        linehumid.setData(lineData);
//        linehumid.animateX(4000);
        linehumid.invalidate();
    }

    private void warning(){
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_firehome)
                .setTitle("Cảnh báo!")
                .setMessage("Khí gas trong phòng vượt qua giới hạn cho phép")
                .setNegativeButton("Xử lí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        stopService(new Intent(getApplicationContext(),
                                FirebaseService.class));
                    }
                })
                .show();
    }

}
