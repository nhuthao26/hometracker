package com.example.hometracker.View;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hometracker.Database.FirebaseContext;
import com.example.hometracker.Model.Weather;
import com.example.hometracker.R;
import com.example.hometracker.Service.FirebaseService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.tv_temp)
    TextView tvtemp;

    @BindView(R.id.tv_humid)
    TextView tvhumid;

    @BindView(R.id.tv_description)
    TextView tvdescription;

    @BindView(R.id.imv_correct)
    ImageView imvcorrect;

    @BindView(R.id.imv_discorrect)
    ImageView imvdiscorrect;

    @BindView(R.id.imv_detail)
    ImageView imvdetail;

    @BindView(R.id.tv_more)
    TextView tvmore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        init();
        addListenner();
    }

    private void init() {
        Intent serviceIntent = new Intent(this, FirebaseService.class);
//        ContextCompat.startForegroundService(this, serviceIntent);
        startService(serviceIntent);
        FirebaseContext.getInstance().getCurrent(new FirebaseContext.FirebaseCallbackCurrent() {
            @Override
            public void onSuccess(Weather weather) {
                tvtemp.setText(weather.getTemp()+"ºC");
                tvhumid.setText(weather.getHumid()+"%");
            }

            @Override
            public void onFail(String message) {
                showToast(message);
            }
        });

        FirebaseContext.getInstance().getGasCurrent(new FirebaseContext.FirebaseCallbackGas() {
            @Override
            public void onSuccess(String gas) {
                if (gas.equals("1")){
                    imvcorrect.setVisibility(View.GONE);
                    imvdiscorrect.setVisibility(View.VISIBLE);
                    tvdescription.setText("Khí gas trong phòng vượt qua giới hạn cho phép");
                    tvdescription.setTextColor(Color.parseColor("#df4a32"));
                    warning();
                }
                else {
                    imvcorrect.setVisibility(View.VISIBLE);
                    imvdiscorrect.setVisibility(View.GONE);
                    tvdescription.setText("Nhiệt độ, độ ẩm, khí gas trong phòng đạt mức cho phép");
                    tvdescription.setTextColor(Color.parseColor("#4267b2"));
                }
            }

            @Override
            public void onFail(String message) {
                showToast(message);
            }
        });
    }

    private void warning(){
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_firehome)
                .setTitle("Cảnh báo!")
                .setMessage("Khí gas trong phòng vượt qua giới hạn cho phép")
                .setNegativeButton("Xử lí", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        stopService(new Intent(getApplicationContext(),
                                FirebaseService.class));
                    }
                })
                .show();
    }

    private void addListenner(){
        imvdetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               gotoDetail();
            }
        });

        tvmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoDetail();
            }
        });
    }

    private void gotoDetail(){
        Intent intent = new Intent(this, DetailActivity.class);
        startActivity(intent);
    }

    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

}
