package com.example.hometracker.Service;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.hometracker.Database.FirebaseContext;
import com.example.hometracker.Notification.App;
import com.example.hometracker.R;
import com.example.hometracker.View.HomeActivity;

public class FirebaseService extends Service {

    private MediaPlayer mediaPlayer;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI);
        mediaPlayer.setLooping(true);
        Intent notificationIntent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent
                .getActivity(this, 0, notificationIntent, 0);
        Notification notification = new NotificationCompat
                .Builder(this, App.CHANNEL_ID)
                .setContentTitle("Thông báo")
                .setContentText("Nhiệt độ, độ ẩm, khí gas đạt mức cho phép")
                .setSmallIcon(R.drawable.ic_firehome)
                .setContentIntent(pendingIntent)
                .build();
        Notification notification2 = new NotificationCompat
                .Builder(this, App.CHANNEL_ID)
                .setContentTitle("Cảnh báo")
                .setContentText("Khí gas trong phòng vượt qua giới hạn cho phép")
                .setSmallIcon(R.drawable.ic_firehome)
                .setContentIntent(pendingIntent)
                .build();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            startForeground(1, notification);
        }
        FirebaseContext.getInstance().getGasCurrent(new FirebaseContext.FirebaseCallbackGas() {
            @Override
            public void onSuccess(String gas) {
                if (gas.equals("1")){
                    mediaPlayer.start();
                    startForeground(1, notification2);
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        stopForeground(0);
                    }
                }
            }

            @Override
            public void onFail(String message) {

            }
        });

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mediaPlayer.stop();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            stopForeground(0);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
