package com.example.hometracker.Model;

public class Weather {
    private String humid;
    private String temp;

    public Weather(String humid, String temp) {
        this.humid = humid;
        this.temp = temp;
    }

    public String getHumid() {
        return humid;
    }

    public void setHumid(String humid) {
        this.humid = humid;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

}
